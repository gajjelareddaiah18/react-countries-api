import React, { useEffect, useState } from "react";
import Search from "./seach"; 
import CountryDataDisplay from "./countryDatadisply"; 
import "./style.css";

export default function CountriesData({ data }) {
  const [regions, setRegions] = useState([]);
  const [subregions, setSubregions] = useState([]);
  const [filterCountries, setFilterCountries] = useState([]);
  const [selectedCountryIndex, setSelectedCountryIndex] = useState(null);
  const [error, setError] = useState(null);
  const [searchTerm, setSearchTerm] = useState("");
  const [selectedRegion, setSelectedRegion] = useState("");
  const [selectedSubRegion, setSelectedSubRegion] = useState("");
  const [sortOrder, setSortOrder] = useState("");
  const [orderArea, setOrderArea] = useState("");
  const [currency, setCurrency] = useState([]);

  useEffect(() => {
    setFilterCountries(data);
    const uniqueRegions = [...new Set(data.map((country) => country.region))];
    setRegions(uniqueRegions);
    const uniqueSubRegions = [...new Set(data.map((country) => country.subregion))];
    setSubregions(uniqueSubRegions);
    const uniqueCurrencies = [...new Set(data.flatMap((country) => 
      country.currencies ? Object.values(country.currencies).map(currency => currency.name) : []))];
    setCurrency(uniqueCurrencies);
  }, [data]);

  const handleRegionChange = (selectedRegion) => {
    setSelectedRegion(selectedRegion);
    setSelectedSubRegion("");
    if (selectedRegion === "") {
      setFilterCountries(data);
    } else {
      const countriesInRegion = data.filter((country) => country.region === selectedRegion);
      setFilterCountries(countriesInRegion);
      const subregionsInRegion = [...new Set(countriesInRegion.map((country) => country.subregion))];
      setSubregions(subregionsInRegion);
    }
  };

  const handleSubRegionChange = (selectedSubRegion) => {
    setSelectedSubRegion(selectedSubRegion);
    if (selectedSubRegion === "") {
      handleRegionChange(selectedRegion); // Reset to countries in selected region
    } else {
      const countriesInSubRegion = data.filter((country) => country.subregion === selectedSubRegion);
      setFilterCountries(countriesInSubRegion);
    }
  };

  const handleSortChange = (selectedSortOption) => {
    setSortOrder(selectedSortOption);
    let sortedCountries = [...filterCountries];
    if (selectedSortOption === "asc") {
      sortedCountries.sort((a, b) => a.population - b.population);
    } else if (selectedSortOption === "desc") {
      sortedCountries.sort((a, b) => b.population - a.population);
    }
    setFilterCountries(sortedCountries);
  };

  const handleSortAreaChange = (selectedSortOption) => {
    setOrderArea(selectedSortOption);
    let sortedCountries = [...filterCountries];
    if (selectedSortOption === "asc") {
      sortedCountries.sort((a, b) => a.area - b.area);
    } else if (selectedSortOption === "desc") {
      sortedCountries.sort((a, b) => b.area - a.area);
    }
    setFilterCountries(sortedCountries);
  };

  const handleCountryChange = (searchTerm) => {
    setSearchTerm(searchTerm);
    setSelectedRegion("");
    setSelectedSubRegion("");
    if (searchTerm === "") {
      setFilterCountries(data);
    } else {
      const filteredCountries = data.filter((country) =>
        country.name.common.toLowerCase().includes(searchTerm.toLowerCase())
      );
      setFilterCountries(filteredCountries);
    }
  };

  const handleCurrencyChange = (selectedCurrency) => {
    if (selectedCurrency === "") {
      setFilterCountries(data);
    } else {
      const countriesWithCurrency = data.filter((country) => 
        country.currencies ? Object.values(country.currencies).some(currency => currency.name === selectedCurrency) : false);
      setFilterCountries(countriesWithCurrency);
    }
  };

  const selectCountry = (index) => {
    setSelectedCountryIndex(index === selectedCountryIndex ? null : index);
  };

  return (
    <div>
      <Search
        regions={regions}
        subregions={subregions}
        handleRegionChange={handleRegionChange}
        handleCountryChange={handleCountryChange}
        handleSubRegionChange={handleSubRegionChange}
        handleSortChange={handleSortChange}
        handleSortAreaChange={handleSortAreaChange}
        handleCurrencyChange={handleCurrencyChange}
        searchTerm={searchTerm}
        currency={currency}
      />
      {error ? (
         <h4 className="errorMessage"><span style={{ color: 'red' }}>{error}</span></h4>
      ) : filterCountries.length === 0 ? (
        <h4 className="errorMessage">No countries found for '<span style={{ color: 'red' }}>{searchTerm}</span>'.</h4>
      ) : (
        <CountryDataDisplay
          filterCountries={filterCountries}
          selectedCountryIndex={selectedCountryIndex}
          selectCountry={selectCountry}
        />
      )}
    </div>
  );
}
