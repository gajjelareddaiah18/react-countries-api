import React from "react";
import { Link } from "react-router-dom"; 

export default function CountryDataDisplay({ filterCountries }) {

  return (
    <div className="countries-container">
      {filterCountries.map((country) => (
        <div className="country" >
          <Link className="link" key={country.cca2} to={`/country/${country.cca2}`}> 
          {/* console.log(country.cca2); */}
            <div className="box">
              <img className="img-2" src={country.flags.png} alt={country.name.common} />
              <h2 className="information">{country.name.common}</h2>
              <p className="information"><strong>Population</strong>: {country.population}</p>
              <p className="information"><strong>Region</strong>: {country.region}</p>
              <p className="information"><strong>Capital</strong>: {country.capital}</p>
              <p className="information"><strong>Currencies</strong>: {country.currencies && Object.values(country.currencies).map((currency, index) => (
    <span key={index}>{currency.name}{index !== country.currencies.length - 1 && ', '}</span>
))}</p>
            </div>
          </Link>
        </div>
      ))}
    </div>
  );
}
