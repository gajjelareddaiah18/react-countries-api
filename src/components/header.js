import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMoon } from '@fortawesome/free-solid-svg-icons';
import "./style.css";

export default function Header() {
    const [isDarkMode, setIsDarkMode] = useState(false);

    const toggleDarkMode = () => {
        setIsDarkMode(!isDarkMode);
        document.body.classList.toggle('dark-mode');
    };

    return (
        <div className="main-heading">
            <div className="header">
                <h1>Where in the World?</h1>
                <div>
                    <FontAwesomeIcon icon={faMoon} />
                    <button className={`toggle-button ${isDarkMode ? 'dark' : 'light'}`} onClick={toggleDarkMode}>Dark Mode</button>
                </div>
            </div>
        </div>
    );
}
