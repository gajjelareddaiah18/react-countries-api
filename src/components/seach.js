import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons'; 
import "./style.css";

export default function Search({ regions, subregions, handleRegionChange, handleCountryChange, handleSubRegionChange, handleSortChange, handleSortAreaChange, handleCurrencyChange, searchTerm, currency }) {
  const [isRegionSelected, setIsRegionSelected] = useState(false); 
  const [sortOption, setSortOption] = useState("");
  const [sortAreaOption, setSortAreaOption] = useState("");
  const [selectedCurrency, setSelectedCurrency] = useState(""); 

  const handleSearch = () => {
    handleCountryChange(searchTerm);
  };

  const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      handleSearch();
    }
  };

  const handleRegionChangeHandler = (value) => {
    handleRegionChange(value);
    setIsRegionSelected(!!value); 
  };

  const handleSubRegionChangeHandler = (value) => {
    handleSubRegionChange(value);
  };

  const handleSort = (e) => {
    const selectedSortOption = e.target.value;
    setSortOption(selectedSortOption);
    handleSortChange(selectedSortOption);
  };

  const handleAreaSort = (e) => {
    const selectedSortOption = e.target.value;
    setSortAreaOption(selectedSortOption);
    handleSortAreaChange(selectedSortOption);
  };

  const handleCurrencyFilter = (e) => {
    const selectedCurrency = e.target.value;
    setSelectedCurrency(selectedCurrency); 
    handleCurrencyChange(selectedCurrency);
  };

  return (
    <div className="mini-header">
      <div className="search-input">
        <FontAwesomeIcon className="icon" icon={faSearch} onClick={handleSearch} />
        <input
          placeholder="Search for a country..."
          type="text"
          value={searchTerm}
          onChange={(e) => handleCountryChange(e.target.value)}
          onKeyDown={handleKeyDown}
        />
      </div>
      
      {regions.length > 0 && (
        <select className='regions' onChange={(e) => handleRegionChangeHandler(e.target.value)}>
          <option value="">Filter by Region</option>
          {regions.map((region, index) => (
            <option key={index} value={region}>
              {region}
            </option>
          ))}
        </select>
      )}

      <select className={`subregions ${isRegionSelected ? 'visible' : ''}`} onChange={(e) => handleSubRegionChangeHandler(e.target.value)}>
        <option value="">Filter by Subregion</option>
        {subregions.map((subregion, index) => (
          <option key={index} value={subregion}>
            {subregion}
          </option>
        ))}
      </select>

      <select onChange={handleSort}>
        <option value="">Sort by Population</option>
        <option value="asc">Population: Low to High</option>
        <option value="desc">Population: High to Low</option>
      </select>

      <select onChange={handleAreaSort}>
        <option value="">Sort by Area</option>
        <option value="asc">Area: Low to High</option>
        <option value="desc">Area: High to Low</option>
      </select>

      <select value={selectedCurrency} onChange={handleCurrencyFilter}>
        <option value="">Filter by Currency</option>
        {Array.isArray(currency) &&
          currency.map((curr, index) => (
            <option key={index} value={curr}>
              {curr}
            </option>
          ))}
      </select>

    </div>
  );
}
